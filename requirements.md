# Requirements


## NPM

Node Package Manager

[Website](https://www.npmjs.com/)

---

## TypeScript

Microsoft TypeScript v4.0+

[Website](https://www.typescriptlang.org//)

    npm install typescript --save-dev

---

## TypeDoc

TypeDoc TypeScript Documentation Generator

[Website](https://typedoc.org/)

    npm install typedoc --save-dev
