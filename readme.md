# ad5


AD5 is an Audio Data capture library used to create visualizations from audio sources. The main class `ad5` can connect to one of three types of audio source and get audio data in an array to be worked with programmatically using the WebAudio API


AD5 is built with TypeScript and can be imported directly in to your project as a simple JavaScript library or module.

---

Version 1.0.0 | VisionMise | 2021