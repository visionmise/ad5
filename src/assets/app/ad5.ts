/**
 * AD5
 * 
 * @summary Audio Data capture library
 * 
 * @description AD5 is an Audio Data capture library used to 
 * create visualizations from audio sources. The
 * main class `ad5` can connect to one of three
 * types of audio source and get audio data in an
 * array to be worked with programmatically using 
 * the WebAudio API
 * 
 * @version 0.1.0
 * @author VisionMise <visionmise@psyn.app>
 * @copyright Geoffrey Kuhl 2021
 * @see https://gitlab.com/visionmise/ad5
 */



/**
 * # Capture Mode
 * Set the audio capture mode
 */
export enum captureMode {

    /**
     * #### Audio Input
     * @description Mic, Input Device, or Line In
     */
    audioInput          = 1,

    /**
     * #### Screen Audio Output
     * @description Screen Capture / All Computer Audio
     */
    screenAudioOutput   = 2,

    /**
     * #### Provided Audio File
     * @description Not Implemented
     */
    audioSourceFile     = 3
}



/**
 * # AD5
 * @summary Audio Capture Class
 * @version 0.1.0
 * @author VisionMise <visionmise@psyn.app>
 * @exports ad5
 */
export class ad5 {

    private capture_mode:captureMode;
    private audio_context:AudioContext;
    private audio_source:MediaStreamAudioSourceNode;
    private audio_analyser:AnalyserNode;
    private audio_options:AnalyserOptions;
    private readyEvent:Event;



    /**
     * ## Constructor
     * @param mode      Audio Capture Mode
     * @param options   Audio API Analyser Options
     */
    constructor(mode:captureMode, options:AnalyserOptions) {

        //New On-Ready Event
        this.readyEvent     = new Event('ad5_ready');

        //Set Capture Mode
        this.capture_mode   = mode;

        //Set Options
        this.audio_options  = options;

        //Set Context
        this.audio_context  = new AudioContext();

        //Start
        this.init();
    }



    /**
     * ## Start AD5 
     * @description Starts the AD5 Audio Capture Controller
     * @async
     */
    private async init() {

        //check if audio context was suspended
        //if so, resume
        if (this.audio_context.state == 'suspended') await this.audio_context.resume();

        //Get audio source based on mode
        this.audio_source       = await this.getSource();

        //create a new audio analyser
        this.audio_analyser     = this.initAnalyser();

        //link the source and the analyser
        this.source.connect(this.analyser);

        //broadcast ready
        dispatchEvent(this.readyEvent);
    }



    /**
     * ## Initialize Analyser
     * @description Creates a new Analyser using *this.context*
     * and *this.audio_options*
     */
    private initAnalyser() {

        //return a new Analyser using this context and options
        return new AnalyserNode(this.context, this.audio_options);
    }



    /**
     * ## Mode
     * @description The current audio capture mode
     * @property 
     * @readonly 
     */
    get mode() : captureMode {
        return this.capture_mode;
    }



    /**
     * ## Context
     * @description The current audio context object
     * @property
     * @readonly
     */
    get context() : AudioContext {
        return this.audio_context;
    }



    /**
     * ## Source
     * @description The current audio source node
     * @property
     * @readonly
     */
    get source() : MediaStreamAudioSourceNode {
        return this.audio_source;
    }



    /**
     * ## Analyser
     * @description The current audio analyser node
     * @property
     * @readonly
     */
    get analyser() : AnalyserNode {
        return this.audio_analyser;
    }



    /**
     * ## Frequency Data
     * @description A snapshot of audio information based
     * on frequency
     * @property
     * @readonly
     */
    get frequencyData() : Uint8Array {

        //set the buffer to the size of the
        //bin data
        const bufferSize    = this.analyser.frequencyBinCount;

        //create a new empty buffer
        const buffer        = new Uint8Array(bufferSize);

        //populate buffer with "current" data
        this.analyser.getByteFrequencyData(buffer);

        //return data buffer
        return buffer;
    }



    /**
     * ## Time Data
     * @description A snapshot of audio information based
     * on temperal progress
     * @property
     * @readonly
     */
    get timeData() : Uint8Array {

        //set the buffer to the size of the
        //bin data
        const bufferSize    = this.analyser.frequencyBinCount;

        //create a new empty buffer
        const buffer        = new Uint8Array(bufferSize);

        //populate buffer with "current" data
        this.analyser.getByteTimeDomainData(buffer);

        //return data buffer
        return buffer;
    }



    /**
     * ## Bandwidth
     * @description The calculated bandwidth based on the 
     * current fidelity and buffer length of the audio source
     * @property
     * @readonly
     */
    get bandwidth() : number {

        //get size of audio buffer
        let size        = this.audio_options?.fftSize || 0;

        //determine bandwidth (size of each band)
        let bandwidth   = this.audio_context.sampleRate / size;

        //return calculated width
        return bandwidth || 0;
    }



    /**
     * ## Wait For Init
     * Waits up to 60 seconds for the 
     * Ready event to fire 
     */
    public waitForInit() : Promise<void> {
        return new Promise((resolve, reject) => {            

            //Set Timeout for 60 Seconds
            setTimeout(() => {reject();}, 60000);

            //Listen for Ready Event
            addEventListener('ad5_ready', () => {
                resolve();
            });

        });
    };



    private waitForInput() : Promise<void> {
        return new Promise((resolve, reject) => {

            setTimeout(() => {
                alert("Press Any Key to Start");
                console.log("No User Input for 30 seconds");
            }, 30000);

            document.addEventListener('keydown', () => {
                resolve();
                console.log("Keyboard Input");
            });

            document.addEventListener('mousemove', () => {
                resolve();
                console.log("Mouse Input");
            });

        });
    }



    /**
     * ## Get Source
     * @description Gets the audio source as an 
     * asynchronous stream
     * @async
     * @returns Promise of a MediaStreamSourceNode
     */
    private async getSource() : Promise<MediaStreamAudioSourceNode> {

        //Create a MediaStream buffer
        let stream:MediaStream;

        //switch case mode
        switch (this.mode) {

            //Not Implemented Yet
            case captureMode.audioSourceFile:
            break;

            //Screen Capture
            case captureMode.screenAudioOutput:

                // @ts-ignore
                stream = await navigator.mediaDevices.getDisplayMedia({
                    video: {

                        // @ts-ignore
                        cursor: 'always'
                    },
                    audio: {
                        echoCancellation: true,

                        // @ts-ignore
                        noiseSuppression: true,

                        sampleRate: 44100
                    }
                });
            break;


            //Audio Input (Line In/Input Device/Mic)
            default:
            case captureMode.audioInput:
                //Get from user devices
                stream = await navigator.mediaDevices.getUserMedia({audio: true});
            break;
        }

        //get source from line input stream
        return this.context.createMediaStreamSource(stream);
    }



    /**
     * ## Part
     * @description Returns the sum of *a* and *b* divided 
     * by *2* unless the sum *(divided by 2)* is greater 
     * than *c* in which case *0* is returned
     * @param a Some Number
     * @param b Some Number
     * @param c Max Sum
     * @returns (*a*+*b*) / 2 or *0*
     */
    public part(a:number, b:number, c:number) : number {
        let binData    = (a + b) / 2;
        return (binData > c) ? binData : 0;
    }



    /**
     * ## Frequency Center
     * @description Returns a given *index* - (*bandwidth* / 2)
     * to *index* + (*bandwidth* / 2) which is the center of a
     * bell curve
     * @param index Audio Snapshot Buffer Index
     * @param buffer Audio Snapshot Buffer
     * @returns Center of a frequency band
     */
    public frequencyCenter(index:number, buffer:Uint8Array) : number {

        let frequency   = ((index + 1) * this.bandwidth);
        let pin         = this.frequencyRange(index, buffer);
        let diff        = pin[1] - pin[0];
        let center      = frequency - (diff / 2);

        return center;
    }



    /**
     * ## Frequency Range
     * @description returns the minimum and maximum 
     * ranges of the given band by index
     * @param index Audio Snapshot Buffer Index
     * @param buffer Audio Snapshot Buffer
     * @returns Min and Max of Band
     */
    public frequencyRange(index:number, buffer:Uint8Array) : Array<number> {
        let min = (index * this.bandwidth);
        let max = ((index + 1) * this.bandwidth);
        return [min, max];
    }



}