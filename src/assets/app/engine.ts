import { ad5 } from "./ad5";

export class engine {

    private fps:number = 75;


    drawSpectrum100(controller:ad5, element:HTMLElement) {

        const audioData:Uint8Array  = controller.frequencyData;        

        let left:number             = 0;

        //f = frequency
        //d = decibels
        //l = label
        for (let f = 0; f < audioData.length; f++) {

            let d = audioData[f];
            let l = controller.frequencyCenter(f, audioData).toFixed(0);

            let barL1   = document.getElementById(`fl1_${l}`);
            let barL2   = document.getElementById(`fl2_${l}`);
            let barR1   = document.getElementById(`fr1_${l}`);
            let barR2   = document.getElementById(`fr2_${l}`);

            if (!barL1) {
                barL1                = document.createElement('div');

                barL1.style.left     = `calc(50% - ${left}px)`;
                barL1.id             = `fl1_${l}`;
                barL1.title          = `${l}`;

                barL1.classList.add('bar');
                element.appendChild(barL1);
            }

            barL1.style.height = `${d / 3}px`;
            barL1.style.bottom = `calc(50% + ${d}px`;
            barL1.style.filter = `hue-rotate(${d * 1}deg) brightness(${d / 1}%)`;

            if (!barL2) {
                barL2                = document.createElement('div');

                barL2.style.left     = `calc(50% - ${left}px)`;
                barL2.id             = `fl2_${l}`;
                barL2.title          = `${l}`;

                barL2.classList.add('bar');
                element.appendChild(barL2);
            }

            barL2.style.height = `${d / 3}px`;
            barL2.style.bottom = `calc(50% - ${d}px`;
            barL2.style.filter = `hue-rotate(${d * 1}deg) brightness(${d / 1}%)`;


            if (!barR1) {
                barR1                = document.createElement('div');

                barR1.style.left     = `calc(50% + ${left}px)`;
                barR1.id             = `fr1_${l}`;
                barR1.title          = `${l}`;

                barR1.classList.add('bar');
                element.appendChild(barR1);
            }

            barR1.style.height = `${d / 3}px`;
            barR1.style.bottom = `calc(50% + ${d}px`;
            barR1.style.filter = `hue-rotate(${d * 1}deg) brightness(${d / 1}%)`;

            if (!barR2) {
                barR2                = document.createElement('div');

                barR2.style.left     = `calc(50% + ${left}px)`;
                barR2.id             = `fr2_${l}`;
                barR2.title          = `${l}`;

                barR2.classList.add('bar');
                element.appendChild(barR2);
            }

            barR2.style.height = `${d / 3}px`;
            barR2.style.bottom = `calc(50% - ${d}px`;
            barR2.style.filter = `hue-rotate(${d * 1}deg) brightness(${d / 1}%)`;

            left += 12;
        }

        setTimeout(() => {

            requestAnimationFrame(() => {
                this.drawSpectrum100(controller, element);
            });

        }, 1000 / this.fps);
    }




    drawSpectrum200(controller:ad5, element:HTMLElement) {

        const audioData:Uint8Array  = controller.timeData;        

        let left:number             = 0;

        //f = frequency
        //d = decibels
        //l = label
        for (let f = 0; f < audioData.length; f++) {

            let d = audioData[f];
            let l = controller.frequencyCenter(f, audioData).toFixed(0);

            let barL1   = document.getElementById(`fl1_${l}`);
            if (!barL1) {
                barL1                = document.createElement('div');

                barL1.style.left     = `calc(50% - ${left}px)`;
                barL1.id             = `fl1_${l}`;
                barL1.title          = `${l}`;

                barL1.classList.add('bar');
                element.appendChild(barL1);
            }

            barL1.style.height = `${d / 3}px`;
            barL1.style.bottom = `calc(50% + ${d}px`;
            barL1.style.filter = `hue-rotate(${d * 1}deg) brightness(${d / 1}%)`;
            left += 12;
        }

        setTimeout(() => {

            requestAnimationFrame(() => {
                this.drawSpectrum100(controller, element);
            });

        }, 1000 / this.fps);
    }


    drawMedLines(controller:ad5) {


        const audioData:Uint8Array  = controller.frequencyData;
        const bassLine:HTMLElement  = document.getElementById('bass');
        const midLine:HTMLElement   = document.getElementById('mid');
        const trebLine:HTMLElement  = document.getElementById('treble');        


        let bassG   = (controller.part(audioData[0], audioData[0], 1));
        let midG    = (controller.part(audioData[4], audioData[3], 1));
        let trebG   = (controller.part(audioData[5], audioData[10], 1));

        // console.log(bassG, midG, trebG);

        let d = (bassG * 5);
        let r = (d / 2);
                
        bassLine.style.height   = `${d}px`;
        bassLine.style.top      = `calc(50% - ${r}px)`;
        bassLine.style.width    = `${d}px`;
        bassLine.style.left     = `calc(50% - ${r}px)`;
        bassLine.style.transform= `rotate(${((midG + trebG) * 1.75 - (bassG * 2)) / 25}deg)`;


        midLine.style.width     = `${midG * 4}px`;
        midLine.style.left      = `${midG * 2}px`;
        midLine.style.top       = `0px`;
        midLine.style.height    = `100%`;


        trebLine.style.width     = `${trebG * 4}px`;
        trebLine.style.left      = `calc(100% - ${trebG * 8}px)`;
        trebLine.style.top       = `0px`;
        trebLine.style.height    = `100%`;
        

        // trebLine.style.height   = `200%`;
        // trebLine.style.top      = `0px`;
        // trebLine.style.right    = `calc(100% - ${trebG * h}px)`;
        // trebLine.style.width    = `calc(25% + ${trebG * 2}px)`;
        // trebLine.style.transform= `rotate(${trebG / 25}deg)`;

        // x.style.left    = `calc(50% - ${z / 2}px)`;
        // x.style.top     = `calc(0% + ${z / 2}px)`;//`calc(50% - ${z / 2}px)`;
        // x.style.top     = `calc(50% - ${z / 2}px)`;
        // x.style.filter  = `brightness(${y / 1}%)`;

        setTimeout(() => {

            requestAnimationFrame(() => {
                this.drawMedLines(controller);
            });

        }, 1000 / this.fps);

    }
    
}