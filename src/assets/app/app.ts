var audio       = null;
var engine      = null;
var viewport    = null;




/**
 * Anonymous Runtime Scope
 */
(async () => {


    //import AD5 Audio Capture Module
    const audioCapture = await import("./ad5.js");


    //ask user for input mode
    const startMode = () : Promise<number> => {
        return new Promise(resolve => {

            const btnDevice = document.getElementById('start_device');
            const btnScreen = document.getElementById('start_screen');
            const viewport  = document.getElementById('viewport');

            btnDevice.addEventListener('click', () => {
                viewport.innerHTML = '';
                resolve(audioCapture.captureMode.audioInput);
            });

            btnScreen.addEventListener('click', () => {
                viewport.innerHTML = '';
                resolve(audioCapture.captureMode.screenAudioOutput);
            });

        });
    };


    //get audio input mode
    const mode = await startMode();


    //create ad5
    const ad5 = new audioCapture.ad5(mode, {
        "fftSize":                  128,
        "smoothingTimeConstant":    0.33,
        "minDecibels":              -70,
        "maxDecibels":              -1,
        "channelCount":             1,
        "channelCountMode":         "explicit",
        "channelInterpretation":    "discrete"
    });

    


    // const audioController   = await import("./ad5.js");
    // const renderer          = await import("./engine.js");

    // addEventListener('ad5_ready', () => {
    //     engine      = new renderer.engine();
    //     viewport    = document.getElementById('viewport');

    //     engine.drawSpectrum100(audio, viewport);
    //     // engine.drawSpectrum200(audio, viewport);
    //     // engine.drawMedLines(audio);
    // });

    // audio = new audioController.ad5(audioController.captureMode.audioInput, {
    //     "fftSize":                  128,
    //     "smoothingTimeConstant":    0.33,
    //     "minDecibels":              -70,
    //     "maxDecibels":              -1,
    //     "channelCount":             1,
    //     "channelCountMode":         "explicit",
    //     "channelInterpretation":    "discrete"
    // });

    

})();